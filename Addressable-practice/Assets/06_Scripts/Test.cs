using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

public class Test : MonoBehaviour
{
    [SerializeField] private Image mIMG;
    AsyncOperationHandle Handle; // 캐싱. 메모리 해제용

    // 로드 버튼 클릭 시
    public void _ClickLoad()
    {
        // 이벤트 함수
        // 버튼 클릭(=함수 실행)되면 "미쿠"이름의 스프라이트를 로드 -> 완료되면(Completed) -> 이벤트 등록(+=)
        // -> 이벤트는 람다식으로 등록 -> 전역변수 Handle 에 값 저장을 위해 람다식 형태 지정(Async..<sprite>
        Addressables.LoadAssetAsync<Sprite>("미쿠").Completed +=
            (AsyncOperationHandle<Sprite> Obj) =>
            {
                Handle = Obj;
                mIMG.sprite = Obj.Result;
            };
    }
    
    // 언로드 버튼 클릭 시 메모리에서 직접적으로 내려줌 *반드시 필요
    public void _ClickUnload()
    {
        Addressables.Release(Handle);
        mIMG.sprite = null;
    }
}
