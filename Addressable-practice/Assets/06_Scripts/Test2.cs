using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class Test2 : MonoBehaviour
{
    [SerializeField] GameObject obj;
    [SerializeField] AssetReference Ref;
    
    // Start is called before the first frame update
    void Start()
    {
        Ref.InstantiateAsync(obj.transform);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
