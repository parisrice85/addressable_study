using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class Create : MonoBehaviour
{
    public void _CreateMiku()
    {
        Addressables.InstantiateAsync("미쿠프리팹", Vector3.zero, Quaternion.identity);
    }
}
