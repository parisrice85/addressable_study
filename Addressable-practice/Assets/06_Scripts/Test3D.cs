using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class Test3D : MonoBehaviour
{
    [SerializeField] AssetReference Ref;
    private GameObject TempObj;
    

    void Start()
    {
        // LoadAsset + Instantiate = InstantiateAsync
        Ref.InstantiateAsync(new Vector3(0, -3, 0), Quaternion.identity).Completed +=
            (AsyncOperationHandle<GameObject> obj) =>
            {
                TempObj = obj.Result;
                
                Invoke(nameof(ReleaseDestroy),3f);
            };
    }

    private void ReleaseDestroy()
    {
        Ref.ReleaseInstance(TempObj);
    }
    
}
